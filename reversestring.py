#!/usr/bin/env python3

""" Enter a string and the program will reverse and print it out. """

def revString(input_str):
    length = len(input_str) - 1
    result = ""

    while length >= 0:
        result += input_str[length]
        length -= 1

    return result

def main():
    mystring = input("Please enter a string: ")
    result = revString(mystring)
    print(result)

if __name__ == "__main__":
    import sys
    sys.exit(main())