#!/usr/bin/env python3

# Create the main function.
def main():
    """ A simple hello world program. """
    print("Hello, World!")

if __name__ == "__main__":
    import sys
    sys.exit(main())
