#!/usr/bin/env python3

""" Pig Latin is a game of alterations played on the English language game. """

def pigLatin(word):
    if word[0] in "aeiou":
        return "".join([word, "ay"])
    else:
        return "".join([word[1:], word[0], "ay"])

def main():
    myword = input("Please enter a word: ")
    result = pigLatin(myword)
    print(result)

if __name__ == "__main__":
    import sys
    sys.exit(main())