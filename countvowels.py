#!/usr/bin/env python3

""" This program counts the vowel in a strings. """

def countVowel(input_str):
    count_a = 0
    count_e = 0
    count_i = 0
    count_o = 0
    count_u = 0
    for char in input_str:
        if char in "aeiou":
            if char == "a":
                count_a += 1
            elif char == "e":
                count_e += 1
            elif char == "i":
                count_i += 1
            elif char == "o":
                count_o += 1
            else:
                count_u += 1
    total = count_a + count_e + count_i + count_o + count_u
    return (total, count_a, count_e, count_i, count_o, count_u)
            
def main():
    mystring = input("Please enter a string: ")
    result = countVowel(mystring)
    print("Total vowels: %d" % result[0])
    print("Total vowel a: %d" % result[1])
    print("Total vowel e: %d" % result[2])
    print("Total vowel i: %d" % result[3])
    print("Total vowel o: %d" % result[4])
    print("Total vowel u: %d" % result[5])

if __name__ == "__main__":
    import sys
    sys.exit(main())
