#!/usr/bin/env python3

""" A simple hello world program with grahical user interface. """

# Import Tkinter graphical user interface toolkit.
import tkinter as tk

class HelloWorld(tk.Frame):

    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
        self.pack(expand=True, fill="both", padx=100, pady=100)
        self.master.title("Hello")
        self.setupUI()

    def setupUI(self):
        hello_lbl = tk.Label(self, text="Hello, World!",
                             fg="red", font=("Times", 14, "bold"))
        hello_lbl.pack()

def main():
    app = HelloWorld()
    app.mainloop()


if __name__ == "__main__":
    import sys
    sys.exit(main())